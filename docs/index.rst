###################################
Welcome to OpenREM's documentation!
###################################

.. image:: openrem0105.png
    :width: 105px
    :align: left
    :height: 105px
    :alt: OpenREM logo

OpenREM is an opensource framework created for the purpose of radiation 
exposure monitoring. The software is capable of importing and displaying 
data from a wide variety of x-ray dose related sources, and then enables 
easy export of the data in a form that is suitable for further analysis 
by suitably qualified medical physics personnel.

Please see `openrem.org <http://openrem.org>`_ for more details.


Contents:

..  toctree::
    :maxdepth: 2

    install
    release-0.7.0
    releasenotes
    import
    netdicom
    i_navigate
    charts
    i_exporting
    openskin
    i_administration
    code

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

