#######
OpenREM
#######

OpenREM is a Django app to extract, store and export Radiation Exposure
Monitoring related information, primarily from DICOM files.

This is a developer release of 0.7.0 - please don't use in production, documentation is not yet ready.

Full documentation can be found on Read the Docs: http://docs.openrem.org

**For upgrades**, please look at the `version release notes <http://docs.openrem.org/en/latest/release-0.7.0.html>`_

For fresh installs, please look at the `install docs <http://docs.openrem.org/page/install.html>`_

Contribution of code, ideas, bug reports documentation is all welcome.
Please feel free to fork the repository and send me pull requests. See
`the website <http://openrem.org/getinvolved>`_ for more information.
