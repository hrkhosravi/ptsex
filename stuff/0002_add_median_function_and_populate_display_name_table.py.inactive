# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
from django.db.models.loading import get_model
from datetime import datetime


def populate_unique_equipment_names(self, orm):

    study_data = self.get_model('remapp','GeneralStudyModuleAttr')

    unique_combinations = study_data.objects.values_list('generalequipmentmoduleattr__manufacturer',
                                                         'generalequipmentmoduleattr__institution_name',
                                                         'generalequipmentmoduleattr__station_name',
                                                         'generalequipmentmoduleattr__institutional_department_name',
                                                         'generalequipmentmoduleattr__manufacturer_model_name',
                                                         'generalequipmentmoduleattr__device_serial_number',
                                                         'generalequipmentmoduleattr__software_versions',
                                                         'generalequipmentmoduleattr__gantry_id').distinct()

    from remapp.models import UniqueEquipmentNames
    from remapp.models import GeneralEquipmentModuleAttr

    for each_combination in unique_combinations:

        if each_combination[1] and each_combination[2]:
            display_name_text = each_combination[1] + ' ' + each_combination[2]
        elif each_combination[1]:
            display_name_text = each_combination[1]
        elif each_combination[2]:
            display_name_text = each_combination[2]
        else:
            display_name_text = "Blank"

        new_combination = UniqueEquipmentNames(manufacturer=each_combination[0],
                                               institution_name=each_combination[1],
                                               station_name=each_combination[2],
                                               institutional_department_name=each_combination[3],
                                               manufacturer_model_name=each_combination[4],
                                               device_serial_number=each_combination[5],
                                               software_versions=each_combination[6],
                                               gantry_id=each_combination[7],
                                               display_name=display_name_text
                                               )
        new_combination.save()

        # Code below here to find every occurrence of the current combination that exists in GeneralStudyModuleAttr
        # and write the appropriate UniqueEquipmentNames row value into the unique_equipment_name field of
        # GeneralStudyModuleAttr.
        equipment_data = GeneralEquipmentModuleAttr.objects.filter(manufacturer=each_combination[0],
                                                                   institution_name=each_combination[1],
                                                                   station_name=each_combination[2],
                                                                   institutional_department_name=each_combination[3],
                                                                   manufacturer_model_name=each_combination[4],
                                                                   device_serial_number=each_combination[5],
                                                                   software_versions=each_combination[6],
                                                                   gantry_id=each_combination[7])

        for row in equipment_data:
            row.unique_equipment_name = UniqueEquipmentNames(pk=new_combination.pk)
            row.save()


def populate_study_workload_chart_time(self, orm):

    test = self.get_model('remapp', 'GeneralStudyModuleAttr')

    for studyData in test.objects.all():
        studyDate = datetime.date(datetime(1900,1,1,0,0,0,0))
        studyTime = studyData.study_time
        if studyTime:
            studyDatetime = datetime.combine(studyDate, studyTime)
        else:
            studyDatetime = None
        studyData.study_workload_chart_time = studyDatetime
        studyData.save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('remapp', '0001_initial'),
    ]

    if 'postgresql' in settings.DATABASES['default']['ENGINE']:
        operations = [
            migrations.AddField('generalstudymoduleattr', 'study_workload_chart_time', models.DateTimeField(blank=True, null=True)),
            migrations.RunPython(populate_study_workload_chart_time),
            migrations.AddField('userprofile', 'median_available', models.BooleanField(default=False)),
            migrations.AddField('userprofile', 'plotAverageChoice', models.CharField(default='mean', max_length=6)),
            migrations.AddField('userprofile', 'plotCTRequestMeanDLP', models.BooleanField(default=False)),
            migrations.AddField('userprofile', 'plotCTRequestFreq', models.BooleanField(default=False)),
            migrations.RunSQL(
                "CREATE FUNCTION _final_median(anyarray) RETURNS NUMERIC AS $$"
                "  WITH q AS"
                "  ("
                "     SELECT val"
                "     FROM UNNEST($1) val"
                "     WHERE val IS NOT NULL"
                "     ORDER BY 1"
                "  ),"
                "  cnt AS"
                "  ("
                "    SELECT COUNT(*) AS c FROM q"
                "  )"
                "  SELECT AVG(val * 10000000000.0)"
                "  FROM"
                "  ("
                "    SELECT val FROM q"
                "    LIMIT  2 - MOD((SELECT c FROM cnt), 2)"
                "    OFFSET GREATEST(CEIL((SELECT c FROM cnt) / 2.0) - 1, 0)"
                "  ) q2;"
                "$$ LANGUAGE SQL IMMUTABLE;"
                "CREATE AGGREGATE median(anyelement) ("
                "  SFUNC=array_append,"
                "  STYPE=anyarray,"
                "  FINALFUNC=_final_median,"
                "  INITCOND='{}'"
                ");",
                "DROP AGGREGATE median(anyelement);"
                "DROP FUNCTION _final_median(anyarray);"
            ),
            migrations.RunPython(populate_unique_equipment_names),
        ]
    else:
        operations = [
            migrations.AddField('generalstudymoduleattr', 'study_workload_chart_time', models.DateTimeField(blank=True, null=True)),
            migrations.RunPython(populate_study_workload_chart_time),
            migrations.AddField('userprofile', 'median_available', models.BooleanField(default=False)),
            migrations.AddField('userprofile', 'plotAverageChoice', models.CharField(default='mean', max_length=6)),
            migrations.AddField('userprofile', 'plotCTRequestMeanDLP', models.BooleanField(default=False)),
            migrations.AddField('userprofile', 'plotCTRequestFreq', models.BooleanField(default=False)),
            migrations.RunPython(populate_unique_equipment_names),
        ]